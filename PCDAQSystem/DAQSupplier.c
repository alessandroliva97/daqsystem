#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <stdint.h>
#include <errno.h>
#include <time.h>

#define PORT 8080 
#define SA struct sockaddr
#define PACKETNUM 128
#define MAX_TRANSF 8388608 //Burst 23 -> 2^23

#define TIMEOUT 1000

typedef struct arg_struct {
	pthread_mutex_t *lock;
	int buffsize;
	int connfd;
	char verbose;
} arguments;

void genrand(char *s, int len) {

    static const char alphanum[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (int i = 0; i < len; ++i) {
        s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }
    s[len] = '\0';

}

void *sender(void *params) {
	
	arguments *args = params;
	char data[args->buffsize];
	long unsigned counter = PACKETNUM, i = 0;
	while (i < counter) {
		genrand(data, args->buffsize);
		char* tosend = (char*)&data;
		int remaining = sizeof(data);
		int result = 0;
		int sent = 0;
		while (remaining > 0) {
		    	result = send(args->connfd, tosend + sent, remaining, 0);
		    	if (result > 0) {
		        	remaining -= result;
		        	sent += remaining;
		    	} else if (result < 0) {
		        	printf("[-] Error retrieving configuration from the client\n");
				exit(0);
		    	}		
		}
		i++;
		pthread_mutex_lock(args->lock);
		if (args->verbose > 0) {
			printf("[*] Packet sent");
		}
		if (args->verbose > 1) {
			printf(":		");
			printf("%s", tosend);
		}
		if (args->verbose > 0) {
                	printf("\n");
		}
		pthread_mutex_unlock(args->lock);
	}
	return 0;

}

void *receiver(void *params) {

	arguments *args = params;
	char data[args->buffsize];
	long unsigned counter = PACKETNUM, i = 0;
	while (i < counter) {
                char* recv_buffer = (char*)&data;
		int remaining = sizeof(data);
		int received = 0;
		int result = 0;
		while (remaining > 0) {
    			result = recv(args->connfd, recv_buffer + received, remaining, 0);
    			if (result > 0) {
        			remaining -= result;
        			received += result;
    			} else if (result == 0) {
        			printf("[-] Remote side closed his end of the connection before all data was received\n");
        			exit(0);
    			} else if (result < 0) {
        			printf("[-] Error retrieving configuration from the client\n");
				exit(0);
    			}
		}
		i++;
		pthread_mutex_lock(args->lock);
		if (args->verbose > 0) {
                	printf("[*] Loopback completed");
		}
		if (args->verbose > 1) {
			printf(":		");
			printf("%s", recv_buffer);
		}
		if (args->verbose > 0) {
                	printf("\n");
		}
		pthread_mutex_unlock(args->lock);
        }
	return 0;

}

int main(int argc, char *argv[]) 
{ 
	if (argc < 2 || argc > 4) {
  		printf("\nUsage: DAQTest [buffer size] \nExample: DAQTest 1024\n\n");
		printf("Optional flags:	-v	Verbose output\n");
		printf("		-vv	Very verbose output (also print data content)\n\n");
  		exit(0);
	}

	int buffsize = atoi(argv[1]);
        if (buffsize < 0 || buffsize > MAX_TRANSF) {
                printf("[-] Invalid buffer size\n");
                exit(0);
        }
	char verbose = 0;
	if (3 == argc) {
		if (strcmp(argv[2], "-v") == 0) {
			verbose = 1;
		} else if (strcmp(argv[2], "-vv") == 0) {
			verbose = 2;
		} else {
			printf("[-] Unwanted parameter\n");
			exit(0);
		}
	}
 
	struct sockaddr_in servaddr, cli; 
	int sockfd = socket(AF_INET, SOCK_STREAM, 0); 
	if (-1 == sockfd) { 
		printf("[-] Socket creation failed\n"); 
		exit(0); 
	}
	bzero(&servaddr, sizeof(servaddr)); 

	servaddr.sin_family = AF_INET; 
	servaddr.sin_addr.s_addr = htons(INADDR_ANY);
	servaddr.sin_port = htons(PORT); 

	if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) { 
		printf("[-] Socket bind failed\n"); 
		exit(0); 
	}
	if ((listen(sockfd, 5)) != 0) { 
		printf("[-] Listen failed\n"); 
		exit(0); 
	}
	socklen_t len = sizeof(cli);
	
	printf("[+] Waiting for client...\n");

	int connfd = accept(sockfd, (SA*)&cli, &len); 
	if (connfd < 0) { 
		printf("[-] Server accept failed\n"); 
		exit(0); 
	}

	struct timeval tv;
	tv.tv_sec = TIMEOUT;
	tv.tv_usec = 0;
	setsockopt(connfd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
	
	printf("[+] Client connected\n");

	char* tosend = (char*)&buffsize;
	int remaining = sizeof(buffsize);
	int result = 0;
	int sent = 0;
	while (remaining > 0) {
    		result = send(connfd, tosend + sent, remaining, 0);
    		if (result > 0) {
        		remaining -= result;
        		sent += remaining;
    		} else if (result < 0) {
        		printf("[-] Error retrieving configuration from the client\n");
			exit(0);
    		}
	}

	pthread_t sendth, recvth;
	pthread_mutex_t lock;
	pthread_mutex_init(&lock, NULL);

	arguments args;
	args.lock = &lock;
	args.buffsize = buffsize;
	args.connfd = connfd;
	args.verbose = verbose;

	clock_t time = clock();

	if (pthread_create(&sendth, NULL, sender, &args)) {
		printf("[-] Error creating sender thread\n");
		exit(0);
	}
	if (pthread_create(&recvth, NULL, receiver, &args)) {
		printf("[-] Error creating receiver thread\n");
		exit(0);
	}

	if (pthread_join(sendth, NULL)) {
		printf("[-] Error joining sender thread\n");
		exit(0);
	}
	if (pthread_join(recvth, NULL)) {
		printf("[-] Error joining receiver thread\n");
		exit(0);
	}

	time = clock() - time;

	printf("[+] Time elapsed: %f seconds\n", ((double)time) / CLOCKS_PER_SEC);

	pthread_mutex_destroy(&lock);
        close(sockfd);

	return 0;

} 

