#
# This file is the axidmatest recipe.
#

SUMMARY = "Simple axidmatest application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://axidmatest.c \
	   file://xdma.h \
	   file://Makefile \
		  "

CFLAGS_prepend = "-I /home/fabrizioalfonsi/Documenti/zybo_0/project-spec/meta-user/recipes-apps/axidmatest/files "

S = "${WORKDIR}"

do_compile() {
	     oe_runmake
}

do_install() {
	     install -d ${D}${bindir}
	     install -m 0755 axidmatest ${D}${bindir}
}
