#
# This file is the DAQFIFO recipe.
#

SUMMARY = "Simple DAQFIFO application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"
CLEANBROKEN = "1"
RDEPENDS_${PN} += "glibc"
ASSUME_SHLIBS = "libpthread.so.0:glibc libc.so.6:glibc"

SRC_URI = "file://DAQFIFO.c \
	   file://Makefile \
		  "

S = "${WORKDIR}"

do_compile() {
	     oe_runmake
}

do_install() {
	     install -d ${D}${bindir}
	     install -m 0755 DAQFIFO ${D}${bindir}
}
