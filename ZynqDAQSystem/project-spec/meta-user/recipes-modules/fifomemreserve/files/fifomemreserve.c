#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <linux/interrupt.h>

#include <linux/of_address.h>
#include <linux/of_device.h>
#include <linux/of_platform.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR
    ("Alessandro Oliva");
MODULE_DESCRIPTION
    ("fifomemreserve - requests mem for DAQFIFO driver");

#define DRIVER_NAME "fifomemreserve"

static int __init lkm_example_init(void) {

	if (request_mem_region(0x40400000, 0x0000ffff, DRIVER_NAME) != NULL) {
		printk(KERN_INFO "*** AXI DMA required range requested successfully\n");
	} else {
		printk(KERN_INFO "*** AXI DMA required range request failed\n");
		return 1;
	}

	if (request_mem_region(0x0a000000, 0x05ffffff, DRIVER_NAME) != NULL) {
		printk(KERN_INFO "*** DAQFIFO required range requested successfully\n");
	} else {
		printk(KERN_INFO "*** DAQFIFO required range request failed\n");
		return 1;
	}

	return 0;

}

static void __exit lkm_example_exit(void) {
	printk(KERN_INFO "*** fifomemreserve exit");
}

module_init(lkm_example_init);
module_exit(lkm_example_exit);
